<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'subjects';

    protected $fillable = ['subjects'];
}
