<?php

namespace App\Http\Controllers\Guru;

use App\User;
use App\Guru;
use App\Mapel;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class HomeAdminController extends Controller
{

    public function home()
    {
        return view('guru.home');
    }

    public function fetchdataAkses(Request $request)
    {
        $id = $request->input('id');
        $admin = User::find($id);
        $output = array (
            'name' => $admin->name,
            'email' => $admin->email,
            'username' => $admin->username,
        );
        echo json_encode($output);
    }

    public function updateAkses(Request $request)
    {
        if ($request->get('check') == 'checked') {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'username' => 'required',
                'password' => ['required', 'confirmed'],
            ]);
        } else {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'username' => 'required',
            ]);
        }
        

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $admin => $messages) {
                $error_array = $messages;

            }
            $output = array(
                'error' => $error_array,
                'success' => $success_output
            );
        } else {
            if($request->get('button_action') == 'update') {
                $akses = User::find($request->get('id'));
                $akses->name = $request->get('name');
                $akses->email = $request->get('email');
                $akses->username = $request->get('username');
                if ($request->get('check') == 'checked') {
                    $akses->password = bcrypt($request->get('password'));
                }
                $akses->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Akses User</div>';
            }
            $output = array(
                'name' => $akses->name,
                'email' => $akses->email,
                'username' => $akses->username,
                'error' => $error_array,
                'success' => $success_output
            );
        }
            
        echo json_encode($output);
    }
    
}
