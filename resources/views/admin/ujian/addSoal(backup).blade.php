@extends('admin.layouts.header-footer')

@section('title-app')
    Aplikasi CBT Online | Tambah Soal Ujian
@stop

@section('css-app')
<style type="text/css">
    .bg-nav-soal {
        background-color: #f0f0f0;
        border-radius: 50%;
        padding: 10px 8px 10px 8px;
        box-shadow: 1px 1px 5px #b4b4b4;
        display: block;
        margin: 0 auto;
        margin-bottom: 30px;
    }
    .bg-nav-soal span {
        text-align: center;
        display: block;
    }
    .bg-nav-soal:hover {
        background-color: #c4c4c4;
        color: #fff;
    }
    .active .bg-nav-soal {
        background-color: #c4c4c4 !important;
        color: #fff !important; 
    }
    .nav-keterangan {
        margin-top: 20px;
    }
    .nav-keterangan .badge {
        margin-bottom: 7px;
        margin-right: 7px;
    }
    .nav-keterangan .badge-light {
        background-color: #f0f0f0;
    }
    .nav-keterangan .badge-success {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 11px 10px 11px;
        border-radius: 50%;
    }
    .nav-keterangan .badge-light {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 13px 10px 13px;
        border-radius: 50%;
    }
    .card-header a.btn,
    .card-header button {
        float: right;
        margin-top: -20px;
        color: #fff;
    }
    .numbering {
        vertical-align: text-top;
    }
    .questions .form-check {
        padding: 10px 25px;
    }
    .carousel-indicators {
        position: relative;
        margin-right: 7%;
        margin-left: 7%;
    }
    .carousel-indicators span {
        width: 30px;
    }
    form label {
        font-weight: bold;
    }
/*    .quest_hidden {
        display: none;
    }*/
</style>
@stop

@section('sidebar-app')
    
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Tambah Soal Ujian</h1>
                <p class="mb-4">(Isilah form berikut dengan benar)</p>

                <!-- DataTales Example -->
                <div id="soalNavigation" class="carousel slide row" data-ride="carousel" data-interval="false">
                    <div class="col-sm-4">
                        <div class="card shadow mb-4" id="data_soal">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Navigasi Soal</h6>
                                <button class="btn btn-primary btn-icon-split btn-sm" id="add_soal" title="Tambah Soal">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-plus"></i>
                                    </span>
                                    <span class="text">Tambah Soal</span>
                                </button>
                            </div>
                            <div class="card-body">
                                <div style="overflow-y: auto; height: 325px;">
                                    <div class="container" style="margin: 10px 0px 10px 0px;">
                                        <div class="row carousel-indicators">
                                            @php
                                                $i = 1;
                                            @endphp
                                            <div class="col-sm-3 active">
                                                <a data-target="#soalNavigation" data-slide-to="0">
                                                    <button class="btn bg-nav-soal">
                                                        <span>{{ $i }}</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="nav-keterangan">
                                    <div class="ket-done">
                                        <span class="badge badge-success">
                                            <i class="fas fa-check"></i>
                                        </span> Soal Sudah Dijawab
                                    </div>
                                    <div class="ket-false">
                                        <span class="badge badge-light">
                                            <i class="fas fa-times"></i>
                                        </span> Soal Belum Dijawab
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <form method="POST" action="{{ url('haiAdmin/ujian/storeSoal') }}" class="form_soal" id="form_soal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Soal Ujian</h6>
                                    <button class="btn btn-primary btn-icon-split btn-sm" id="selesai" title="Selesai">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-check"></i>
                                        </span>
                                        <span class="text">Selesai</span>
                                    </button>
                                </div>
                                <div class="card-body">
                                    <div class="container" style="margin: 10px 0px 10px 0px;">
                                      <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            
                                            @foreach($check as $datas)
                                                <input type="hidden" name="test_id[]" class="test_id" id="test_id" value="{{ $datas->id }}">
                                            @endforeach
                                            <table width="100%">
                                                <tr>
                                                    <td class="numbering">
                                                        <h5>{{ $i }}.</h5>
                                                    </td>
                                                    <td>
                                                        <textarea class="form-control questions" id="questions" name="questions[]" value="" rows="7" placeholder="Soal Ujian" required></textarea>
                                                        <br>
                                                        <input type="checkbox" name="ceklis[]" id="ceklis">
                                                        <label for="ceklis">Upload Gambar untuk Soal?</label>
                                                        <input type="file" name="quest_img[]" class="quest_img" id="quest_img" style="margin-bottom: 50px;">
                                                    </td>
                                                </tr>
                                                @foreach($check as $datas)
                                                @if( $datas->type_test == 'Pilihan Ganda')
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="row questions">
                                                            <div class="col-sm-1">
                                                                <div class="form-check">
                                                                  <!-- <input class="form-check-input" type="radio" name="answer_key" id="a" value="a" required> -->
                                                                  <label class="form-check-label" for="a">A.</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-11">
                                                                <div class="form-group">
                                                                    <textarea type="text" name="a[]" value="" class="form-control a" id="a" placeholder="Jawaban" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="row questions">
                                                            <div class="col-sm-1">
                                                                <div class="form-check">
                                                                  <!-- <input class="form-check-input" type="radio" name="answer_key" id="b" value="b" required> -->
                                                                  <label class="form-check-label" for="b">B.</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-11">
                                                                <div class="form-group">
                                                                    <textarea type="text" name="b[]" value="" class="form-control b" id="b" placeholder="Jawaban" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="row questions">
                                                            <div class="col-sm-1">
                                                                <div class="form-check">
                                                                  <!-- <input class="form-check-input" type="radio" name="answer_key" id="c" value="c" required> -->
                                                                  <label class="form-check-label" for="c">C.</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-11">
                                                                <div class="form-group">
                                                                    <textarea type="text" name="c[]" value="" class="form-control c" id="c" placeholder="Jawaban" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="row questions">
                                                            <div class="col-sm-1">
                                                                <div class="form-check">
                                                                  <!-- <input class="form-check-input" type="radio" name="answer_key" id="d" value="d" required> -->
                                                                  <label class="form-check-label" for="d">D.</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-11">
                                                                <div class="form-group">
                                                                    <textarea type="text" name="d[]" value="" class="form-control d" id="d" placeholder="Jawaban" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="row questions">
                                                            <div class="col-sm-1">
                                                                <div class="form-check">
                                                                  <!-- <input class="form-check-input" type="radio" name="answer_key" id="e" value="e" required> -->
                                                                  <label class="form-check-label" for="e">E.</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-11">
                                                                <div class="form-group">
                                                                    <textarea type="text" name="e[]" value="" class="form-control e" id="e" placeholder="Jawaban" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div class="form-group" style="margin-top: 25px;">
                                                            <label>Kunci Jawaban</label>
                                                            <input type="text" name="answer_key[]" value="" class="form-control answer_key" id="answer_key" placeholder="Kunci Jawaban" required>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @else
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div class="form-group" style="margin-top: 10px;">
                                                            <label>Kunci Jawaban</label>
                                                            <textarea type="text" name="answer_essay[]" value="" class="form-control answer_essay" id="answer_essay" placeholder="Kunci Jawaban" required></textarea>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </table>
                                            <!-- <button class="btn btn-success btn-icon-split btn-sm" id="submit" type="submit" title="Simpan Soal" style="float: right;">
                                                <span class="icon text-white-50">
                                                    <i class="fas fa-check"></i>
                                                </span>
                                                <span class="text">Simpan Soal</span>
                                            </button> -->
                                        </div>
                                      </div>

                                        <br>
                                        <a href="#soalNavigation" data-slide="prev" class="btn btn-secondary btn-icon-split btn-sm" style="float: left;" title="Sebelumnya">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-arrow-left"></i>
                                            </span>
                                            <span class="text">Sebelumnya</span>
                                        </a>
                                        <a href="#soalNavigation" data-slide="next" class="btn btn-primary btn-icon-split btn-sm" style="float: right;" title="Selanjutnya">
                                            <span class="text">Selanjutnya</span>
                                            <span class="icon text-white-50">
                                                <i class="fas fa-arrow-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function () {

        $('#selesai').click(function() {
            var count = 0;
            $('textarea').each(function(){
                if ($(this).val() == "") {
                    alert('gagal');
                    return false;
                }
                
            });
        });

        // var updateSoal = function() {
        //     $('.form_soal').on('submit', function(event) {
        //         event.preventDefault();
        //         $.ajaxSetup({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             }
        //         });
        //         $.ajax({
        //             url:"{{ url('haiAdmin/ujian/storeSoal') }}",
        //             method:"POST",
        //             data:new FormData(this),
        //             contentType:false,
        //             cache:false,
        //             processData:false,
        //             success:function()
        //             {
        //                 alert('Data Tersimpan');
        //             }
        //         })
        //     });
        // } 
        // updateSoal();

        var slide = 0;
        var no = 1;
        
        $('#add_soal').on('click', function() {
            slide++;
            no++;

            var navNumbering = '<div class="col-sm-3">' +
                                    '<a data-target="#soalNavigation" data-slide-to="' + slide + '">' +
                                        '<button class="btn bg-nav-soal">' +
                                            '<span>' + no + '</span>' +
                                        '</button>' +
                                    '</a>' +
                                '</div>';
            $('.carousel-indicators').append(navNumbering);

            var addSoal = '<div class="carousel-item">' +
                            // '<form method="POST" id="form_soal" class="form_soal" enctype="multipart/form-data">' +
                            // '{{ csrf_field() }}' +
                                '@foreach($check as $datas)' +
                                    '<input type="hidden" name="test_id[]" class="test_id" id="test_id" value="{{ $datas->id }}">' +
                                '@endforeach' +
                                '<table width="100%">' +
                                    '<tr>' +
                                        '<td class="numbering">' +
                                            '<h5>' + no + '.</h5>' +
                                        '</td>' +
                                        '<td>' +
                                            '<textarea class="form-control questions" id="questions[]" name="questions" value="" rows="7" placeholder="Soal Ujian" required></textarea>' +
                                            '<br>' +
                                            '<input type="checkbox" name="ceklis[]" id="ceklis">' +
                                            '<label for="ceklis">Upload Gambar untuk Soal?</label>' +
                                            '<input type="file" name="quest_img[]" class="quest_img" id="quest_img" style="margin-bottom: 50px;" multiple="multiple">' +
                                        '</td>' +
                                    '</tr>' +
                                    '@foreach($check as $datas)' +
                                    '@if( $datas->type_test == "Pilihan Ganda")' +
                                    '<tr>' +
                                        '<td>' +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="row questions">' +
                                                '<div class="col-sm-1">' +
                                                    '<div class="form-check">' +
                                                      // '<input class="form-check-input" type="radio" name="answer_key" id="a" value="a" required>' +
                                                      '<label class="form-check-label" for="a">A.</label>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="col-sm-11">' +
                                                    '<div class="form-group">' +
                                                        '<textarea type="text" name="a[]" value="" class="form-control a" id="a" placeholder="Jawaban" required></textarea>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                       '<td>' +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="row questions">' +
                                                '<div class="col-sm-1">' +
                                                   '<div class="form-check">' +
                                                      // '<input class="form-check-input" type="radio" name="answer_key" id="b" value="b" required>' +
                                                      '<label class="form-check-label" for="b">B.</label>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="col-sm-11">' +
                                                    '<div class="form-group">' +
                                                        '<textarea type="text" name="b[]" value="" class="form-control b" id="b" placeholder="Jawaban" required></textarea>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td>' +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="row questions">' +
                                                '<div class="col-sm-1">' +
                                                    '<div class="form-check">' +
                                                      // '<input class="form-check-input" type="radio" name="answer_key" id="c" value="c" required>' +
                                                      '<label class="form-check-label" for="c">C.</label>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="col-sm-11">' +
                                                   '<div class="form-group">' +
                                                        '<textarea type="text" name="c[]" value="" class="form-control c" id="c" placeholder="Jawaban" required></textarea>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                       '<td>' +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="row questions">' +
                                                '<div class="col-sm-1">' +
                                                    '<div class="form-check">' +
                                                      // '<input class="form-check-input" type="radio" name="answer_key" id="d" value="d" required>' +
                                                      '<label class="form-check-label" for="d">D.</label>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="col-sm-11">' +
                                                    '<div class="form-group">' +
                                                        '<textarea type="text" name="d[]" value="" class="form-control d" id="d" placeholder="Jawaban" required></textarea>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td>' +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="row questions">' +
                                                '<div class="col-sm-1">' +
                                                    '<div class="form-check">' +
                                                      // '<input class="form-check-input" type="radio" name="answer_key" id="e" value="e" required>' +
                                                      '<label class="form-check-label" for="e">E.</label>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="col-sm-11">' +
                                                    '<div class="form-group">' +
                                                        '<textarea type="text" name="e[]" value="" class="form-control e" id="e" placeholder="Jawaban" required></textarea>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td></td>' +
                                        '<td>' +
                                            '<div class="form-group" style="margin-top: 25px;">' +
                                                '<label>Kunci Jawaban</label>' +
                                                '<input type="text" name="answer_key[]" value="" class="form-control answer_key" id="answer_key" placeholder="Kunci Jawaban" required>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '@else' +
                                    '<tr>' +
                                        '<td></td>' +
                                        '<td>' +
                                            '<div class="form-group" style="margin-top: 10px;">' +
                                                '<label>Kunci Jawaban</label>' +
                                                '<textarea type="text" name="answer_essay[]" value="" class="form-control answer_essay" id="answer_essay" placeholder="Kunci Jawaban" required></textarea>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>' +
                                    '@endif' +
                                    '@endforeach' +
                                '</table>' +
                                // '<button class="btn btn-success btn-icon-split btn-sm" type="submit" title="Simpan Soal" style="float:right;">' +
                                //     '<span class="icon text-white-50">' +
                                //         '<i class="fas fa-check"></i>' +
                                //     '</span>' +
                                //     '<span class="text">Simpan Soal</span>' +
                                // '</button>' +
                            // '</form>' +
                        '</div>'
            $('.carousel-inner').append(addSoal);

            // updateSoal();
        });

    });
</script>
@stop
