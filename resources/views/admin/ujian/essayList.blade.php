@extends('admin.layouts.header-footer')

@section('css-app')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <style type="text/css">
        td.dur:after {
            content: " menit";
        }
        td.jum:after {
            content: " soal";
        }
        .td,
        .dur,
        .jum {
            width: 10% !important;
        }
        .action {
            width: 15% !important;
        }
        .modal-dialog {
            max-width: 550px;
        }
    </style>
@stop

@section('sidebar-app')
    <li class="nav-item">
      <a class="nav-link" href="{{ route('homeAdmin') }}">
        <i class="fas fa-fw fa-home"></i>
        <span>Home</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
      Menu
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('kelas') }}">
        <i class="fas fa-fw fa-chalkboard"></i>
        <span>Manajemen Kelas</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('mapel') }}">
        <i class="fas fa-fw fa-book"></i>
        <span>Manajemen Pelajaran</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru') }}">
        <i class="fas fa-fw fa-chalkboard-teacher"></i>
        <span>Manajemen Guru</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('siswa') }}">
        <i class="fas fa-fw fa-user-tie"></i>
        <span>Manajemen Siswa</span></a>
    </li>

    <li class="nav-item active">
      <a class="nav-link" href="{{ route('ujian') }}">
        <i class="fas fa-fw fa-book-reader"></i>
        <span>Manajemen Ujian</span></a>
    </li>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Manajemen Ujian</h1>

      @foreach($log as $logs)
        @if($logs->code_test_log == null)

        @else
              <a href="{{ route('ujian.deleteLog') }}" class="btn btn-danger btn-sm" style="float: right; margin-top: -50px;">
                  <i class="fas fa-trash"></i>&nbsp;
                  Hapus Log Ujian
              </a>
        @endif
      @endforeach

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Data Ujian</h6>
          <a href="{{ url('/haiAdmin/ujian/addForm') }}">
              <button class="btn btn-primary btn-sm">
                <i class="fas fa-plus"></i>
                Tambah
              </button>
          </a>
        </div>
        <div class="card-body">
            @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                </div>
            @endif
            @if(Session::has('alert-info'))
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                </div>
            @endif
            @if(Session::has('alert-danger'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                </div>
            @endif
            <span id="form_output"></span>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('ujian') }}"><b>Pilihan Ganda</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="{{ route('ujianEssay') }}"><b>Essay/Uraian</b></a>
              </li>
            </ul>
            <br>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="essay_table" style="width: 100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Kode Ujian</th>
                                <th>Nama Ujian</th>
                                <th>Durasi (menit)</th>
                                <th>Jumlah Soal</th>
                                <th>Tipe Ujian</th>
                                <th>Pelajaran</th>
                                <th>Dibuat Oleh</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <div id="detail_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="detail_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Detail Data Ujian</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Kode Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="code_test" name="id" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nama Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name_test" name="name_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Durasi (menit)<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="duration" name="duration" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Type Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="type_test" name="type_test" class="form-control" readonly>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Pelajaran<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="subjects" name="subjects" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Dibuat Oleh<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name" name="name" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="edit_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Data Ujian</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Kode Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="code_test_edit" name="code_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nama Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name_test_edit" name="name_test" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Durasi (menit)<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="duration_edit" name="duration" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Type Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="type_test_edit" name="type_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Pelajaran<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control" name="subjects_id" id="subjects_id">
                                        <option value="" selected>Pilih</option>
                                        @foreach($mapel as $mapels)
                                            <option value="{{ $mapels->id }}">{{ $mapels->subjects }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Dibuat Oleh<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name_get" name="name" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id_get" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#essay_table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [[ 1, "asc" ]],
                "ajax": "{{ route('ujianEssay.getEssay') }}",
                "columns": [
                    { 
                        "data": "code_test",
                        "className":"td",
                    },
                    { 
                        "data": "name_test",
                        "className":"td",
                    },
                    { 
                        "data": "duration",
                        "className":"dur",
                    },
                    { 
                        "data": "jml_soal",
                        "className":"jum",
                    },
                    { 
                        "data": "type_test",
                        "className":"td",
                    },
                    { 
                        "data": "subjects",
                        "className":"td",
                    },
                    { 
                        "data": "name",
                        "className":"td",
                    },
                    { 
                        "data": "action",
                        "orderable": "false",
                        "searchable": "false",
                        "className":"action",
                    }
                ]
            });

            $('.close, .close-btn').click(function() {
                $('#error_output, #error_profil').hide();
            });

            $('#edit_form').on('submit', function(event) {
                event.preventDefault();

                var form_data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:"{{ route('ujian.updateUjian') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if (data.error.length > 0) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                            }
                            $('#error_output').html(error_html);
                            $('#error_output').show();
                        } else {
                            $('#form_output').html(data.success);
                            $('#edit_form')[0].reset();
                            $('#button_act').val('updated');
                            $('#test_table, #essay_table').DataTable().ajax.reload();
                            $('#edit_modal').modal('hide');
                            $('#form_output').show();
                            $('#error_output').hide();
                            $('#action').show();
                        }
                    }
                })
            });

            $(document).on('click', '.detail', function() {
                var id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('ujian.fetchdataUjian') }}",
                    method:"GET",
                    data:{id:id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#code_test').val(data.code_test);
                        $('#name_test').val(data.name_test);
                        $('#duration').val(data.duration);
                        $('#type_test').val(data.type_test);
                        $('#subjects').val(data.subjects);
                        $('#name').val(data.name);
                        $('#id').val(id);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('ujian.fetchdataEditUjian') }}",
                    method:"GET",
                    data:{id:id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#code_test_edit').val(data.code_test);
                        $('#name_test_edit').val(data.name_test);
                        $('#duration_edit').val(data.duration);
                        $('#type_test_edit').val(data.type_test);
                        $('#subjects_id').val(data.subjects_id);
                        $('#name_get').val(data.name);
                        $('#id_get').val(id);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).attr('id');
                if(confirm("Apakah anda yakin akan menghapus Data ini?"))
                {
                    $.ajax({
                        url:"{{ route('ujian.delete') }}",
                        method:"GET",
                        data:{id:id},
                        success:function(data)
                        {
                            alert(data);
                            $('#test_table, #essay_table').DataTable().ajax.reload();
                            $('#form_output, #error_output, #error_profil').hide();
                        }
                    }) 
                }
                else {
                    return false;
                }
            });
        });
    </script>
@stop
