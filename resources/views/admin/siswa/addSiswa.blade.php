@extends('admin.layouts.header-footer')

@section('css-app')
<style type="text/css">
    .form-group {
        margin-bottom: 25px;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
@stop

@section('sidebar-app')
    <li class="nav-item">
      <a class="nav-link" href="{{ route('homeAdmin') }}">
        <i class="fas fa-fw fa-home"></i>
        <span>Home</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
      Menu
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('kelas') }}">
        <i class="fas fa-fw fa-chalkboard"></i>
        <span>Manajemen Kelas</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('mapel') }}">
        <i class="fas fa-fw fa-book"></i>
        <span>Manajemen Pelajaran</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru') }}">
        <i class="fas fa-fw fa-chalkboard-teacher"></i>
        <span>Manajemen Guru</span></a>
    </li>

    <li class="nav-item active">
      <a class="nav-link" href="{{ route('siswa') }}">
        <i class="fas fa-fw fa-user-tie"></i>
        <span>Manajemen Siswa</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('ujian') }}">
        <i class="fas fa-fw fa-book-reader"></i>
        <span>Manajemen Ujian</span></a>
    </li>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Registrasi User Siswa</h1>
                <br>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Registrasi User Siswa</h6>
                        <a href="{{ url('/haiAdmin/siswa') }}">
                            <button class="btn btn-secondary btn-sm">
                                <i class="fas fa-arrow-left"></i>
                                Kembali
                            </button>
                        </a>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('/haiAdmin/siswa/storeAkses') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="name">Nama Siswa<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nama Siswa" required>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="username">Username<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" placeholder="Username" required>
                                                @if ($errors->has('username'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="email">Email<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="password">Password<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  required>
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                                
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="password-confirm">Konfirmasi Password<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="password-confirm" placeholder="Konfirmasi Password" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <input type="hidden" name="role" class="form-control" value="siswa">

                            <div class="btn-group">
                                <button class="btn btn-primary">
                                    <i class="fas fa-check"></i>
                                    Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
