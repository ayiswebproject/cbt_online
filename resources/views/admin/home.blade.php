@extends('admin.layouts.header-footer')

@section('css-app')
    <style type="text/css">
        #main-content h1 {
            text-align: center;
            font-style: italic;
            font-size: 72pt;
        }
        @media (max-width: 600px) {
            #main-content h1 {
                font-size: 42px;
            }
        }
        #main-content h5 {
            text-align: center;
            font-size: 18pt;
        }
        @media (max-width: 600px) {
            #main-content h5 {
                font-size: 16px;
            }
        }
        #main-content .left {
            float: right;
        }
        .col-sm-6 {
            margin-bottom: 25px;
        }
        .brand-card img {
            width: 65%;
            margin: 0 auto;
            display: block;
        }
        .brand-card {
            padding: 35px 35px;
        }
        @media (max-width: 600px) {
            .brand-card {
                padding: 0px 0px;
            }
        }
        .profil-identity label {
            text-align: center; 
            display: block; 
            font-size: 18px; 
            color: #fff; 
            font-weight: 600;
        }
    </style>
@stop

@section('sidebar-app')
    <li class="nav-item active">
      <a class="nav-link" href="{{ route('homeAdmin') }}">
        <i class="fas fa-fw fa-home"></i>
        <span>Home</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
      Menu
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('kelas') }}">
        <i class="fas fa-fw fa-chalkboard"></i>
        <span>Manajemen Kelas</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('mapel') }}">
        <i class="fas fa-fw fa-book"></i>
        <span>Manajemen Pelajaran</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru') }}">
        <i class="fas fa-fw fa-chalkboard-teacher"></i>
        <span>Manajemen Guru</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('siswa') }}">
        <i class="fas fa-fw fa-user-tie"></i>
        <span>Manajemen Siswa</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('ujian') }}">
        <i class="fas fa-fw fa-book-reader"></i>
        <span>Manajemen Ujian</span></a>
    </li>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Home</h1>

      <div class="row" id="main-content">
          <div class="col-sm-6">
              <div class="card shadow h-100">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Welcome, {{ Auth::user()->name }}</h6>
                </div>
                <div class="card-body">
                    <div class="brand-card">
                        <img src="{{ asset('img/ayocbt_dark.png') }}">
                    </div>
                </div>
              </div>
          </div>
          <div class="col-sm-6">
                <div class="card shadow h-100">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">Profil Saya</h6>
                    </div>
                    <div class="card-body">
                        <span id="form_output"></span>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="card" style="background-color: #4E73DF">
                                    <div class="card-body">
                                        <img src="{{ asset('img/user.png') }}" class="img-profile rounded-circle" style="width: 50% !important;">
                                        <br>
                                        <div class="profil-identity">
                                            <label>{{ Auth::user()->name }}</label>
                                            <span style="font-size: 12px; text-transform: capitalize; color: #fff; display: block; text-align: center;">Role : {{ Auth::user()->role }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="home-profile">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row" style="margin-bottom: 15px;">
                                                <div class="col-sm-3">
                                                    <label class="control-label bold">Nama</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input class="form-control" id="name_show" value="{{ Auth::user()->name }}" readonly>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 15px;">
                                                <div class="col-sm-3">
                                                    <label class="control-label bold">Email</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input class="form-control" id="email_show" value="{{ Auth::user()->email }}" readonly>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 15px;">
                                                <div class="col-sm-3">
                                                    <label class="control-label bold">Username</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input class="form-control" id="username_show" value="{{ Auth::user()->username }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="left">
                            <a href="#" class="btn btn-outline-secondary btn-sm edit-akses" data-toggle="modal" data-target="#akses_modal_home" id="{{ Auth::user()->id }}">
                                <i class="fas fa-unlock-alt"></i>
                                &nbsp;Edit Akses User
                            </a>
                            <!-- <a href="#" class="btn btn-primary btn-icon-split btn-sm">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Edit Profil</span>
                            </a> -->
                        </div>
                    </div>
                </div>
          </div>
      </div>

    </div>
    <!-- /.container-fluid -->

    <div id="akses_modal_home" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="akses_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Akses User</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div id="akses_bio">
                            <div class="form-group">
                                <label>Nama<span style="color:red;">*</span></label>
                                <input type="text" id="nameAkses" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Email<span style="color:red;">*</span></label>
                                <input type="email" id="emailAkses" name="email" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Username<span style="color:red;">*</span></label>
                                <input type="text" id="usernameAkses" name="username" class="form-control">
                            </div>
                        </div>
                        <div id="akses_pswd">
                            <div class="form-group">
                                <label>Password<span style="color: red">*</span></label>
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password<span style="color: red">*</span></label>
                                <input id="password-confirm" type="password" class="form-control pswd-conf" name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group" style="margin-left: 5px;">
                            <input type="checkbox" class="check-pass" name="check" value="">
                            <label for="check_pswd"><b>Ganti Password Anda?</b></label>
                        </div>
                        <input type="hidden" name="role" class="form-control" value="admin">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function() {
        $('.close, .close-btn').click(function() {
            $('#error_output, #error_profil').hide();
        });

        $('#password').prop('disabled', true);
        $('.pswd-conf').prop('disabled', true);
        $('#akses_pswd').hide();

        $(".check-pass").click(function() {
            if($('.check-pass').is(':checked')) { 
                $('#password').prop('disabled', false);
                $('.pswd-conf').prop('disabled', false);
                $('.check-pass').val('checked');
                $('#akses_pswd').show();
            } else {
                $('#password').prop('disabled', true);
                $('.pswd-conf').prop('disabled', true);
                $('.check-pass').val('');
                $('#akses_pswd').hide();
            }
        });

        $('#akses_form').on('submit', function(event) {
            event.preventDefault();

            var form_data = $(this).serialize();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:"{{ route('admin.updateAkses') }}",
                method:"POST",
                data:form_data,
                dataType:"json",
                success:function(data)
                {
                    if (data.error.length > 0) {
                        var error_html = '';
                        for (var count = 0; count < data.error.length; count++) {
                            error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                        }
                        $('#error_output').html(error_html);
                        $('#password').val('');
                        $('#password-confirm').val('');
                        $('#error_output').show();
                    } else {
                        $('#form_output').html(data.success);
                        $('#name_show').val(data.name);
                        $('#email_show').val(data.email);
                        $('#username_show').val(data.username);
                        $('#name-dropdown, .profil-identity label').html(data.name);
                        $('#akses_form')[0].reset();
                        $('#button_action').val('update');
                        $('#akses_modal_home').modal('hide');
                        $('#form_output').show();
                        $('#error_output').hide();
                        $('#action').show();
                        $('#password').prop('disabled', true);
                        $('.pswd-conf').prop('disabled', true);
                        $('#akses_pswd').hide();
                    }
                }
            })
        });

        $(document).on('click', '.edit-akses', function() {
            var id = $(this).attr('id');
            $.ajax({
                url:"{{ route('admin.fetchdataAkses') }}",
                method:"GET",
                data:{id:id},
                dataType:"json",
                success:function(data)
                {
                    $('#nameAkses').val(data.name);
                    $('#emailAkses').val(data.email);
                    $('#usernameAkses').val(data.username);
                    $('#pswd_hide').val(data.pswd_hide);
                    $('#action').val('Edit');
                    $('.modal-title').text('Edit Akses User');
                    $('#id').val(id);
                    $('#form_output').hide();
                }
            })
        });
    });
</script>
@stop
