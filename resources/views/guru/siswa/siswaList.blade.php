@extends('admin.layouts.header-footer')

@section('css-app')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<style type="text/css">
    .form-group {
        margin-bottom: 25px;
    }
    .modal-dialog {
        max-width: 550px;
    }
</style>
@stop

@section('sidebar-app')
    <li class="nav-item">
      <a class="nav-link" href="{{ route('homeGuru') }}">
        <i class="fas fa-fw fa-home"></i>
        <span>Home</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
      Menu
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru.kelas') }}">
        <i class="fas fa-fw fa-chalkboard"></i>
        <span>Manajemen Kelas</span></a>
    </li>

    <li class="nav-item active">
      <a class="nav-link" href="{{ route('guru.siswa') }}">
        <i class="fas fa-fw fa-user-tie"></i>
        <span>Manajemen Siswa</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru.ujian') }}">
        <i class="fas fa-fw fa-book-reader"></i>
        <span>Manajemen Ujian</span></a>
    </li>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Manajemen Siswa</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Data Siswa</h6>
          <a href="{{ url('/haiGuru/siswa/addForm') }}">
              <button class="btn btn-success btn-sm">
                <i class="fas fa-user"></i>
                Registrasi User
              </button>
          </a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                </div>
            @endif
            @if(Session::has('alert-info'))
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                </div>
            @endif
            @if(Session::has('alert-danger'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                </div>
            @endif
            <span id="form_output"></span>
            <table class="table table-bordered" id="siswa_table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nama Siswa</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>No. Induk</th>
                        <th>Gender</th>
                        <th>Kelas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    <div id="detail_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="detail_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Detail Profil Siswa</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nama Siswa<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="name" name="name" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Email<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="email" id="email" name="email" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Username<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="username" name="username" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Jenis Kelamin<span style="color:red;">*</span></label>
                                    
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="gender" name="gender" class="form-control" readonly>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>No. Induk<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="no_induk" name="no_induk" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Kelas<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="class" name="class" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="akses_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="akses_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Akses User</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div id="akses_bio">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Nama Siswa<span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" id="nameAkses" name="name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Email<span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="email" id="emailAkses" name="email" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Username<span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" id="usernameAkses" name="username" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="akses_pswd">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Password<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input id="password" type="password" class="form-control" name="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Konfirmasi Password<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input id="password-confirm" type="password" class="form-control pswd-conf" name="password_confirmation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-left: 5px;">
                            <input type="checkbox" class="check-pass" name="check" value="">
                            <label for="check_pswd"><b>Ganti Password Anda?</b></label>
                        </div>
                        <input type="hidden" name="role" class="form-control" value="guru">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="profil_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="profil_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Profil Siswa</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_profil"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="hidden" name="id" id="id_prof">
                                    <label>Jenis Kelamin</label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control" name="gender" id="genderProf">
                                        <option value="" selected>Pilih</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>No. Induk Guru</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="no_induk" class="form-control" id="no_indukProf">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Mata Pelajaran</label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control" name="class_id" id="class_id">
                                        <option value="" selected>Pilih</option>
                                        @foreach($class as $classes)
                                            <option value="{{ $classes->id }}">{{ $classes->class }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="user_id" id="id_pfl" value="">
                        <input type="hidden" name="button_act" id="button_act" value="updated">
                        <input type="submit" name="submit" id="act" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('js/datatables-custom.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#siswa_table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [[ 3, "asc" ]],
                "ajax": "{{ route('guru.siswa.getData') }}",
                "columns": [
                    { 
                        "data": "name",
                        "width": "10%"
                    },
                    { 
                        "data": "username",
                        "width": "10%"
                    },
                    { 
                        "data": "email",
                        "width": "10%"
                    },
                    { 
                        "data": "no_induk",
                        "width": "10%"
                    },
                    { 
                        "data": "gender",
                        "width": "10%"
                    },
                    { 
                        "data": "class",
                        "width": "10%"
                    },
                    { 
                        "data": "action",
                        "orderable": "false",
                        "searchable": "false",
                        "width": "15%"
                    }
                ]
            });

            $('.close, .close-btn').click(function() {
                $('#error_output, #error_profil').hide();
            });

            $('#password').prop('disabled', true);
            $('.pswd-conf').prop('disabled', true);
            $('#akses_pswd').hide();

            $(".check-pass").click(function() {
                if($('.check-pass').is(':checked')) { 
                    $('#password').prop('disabled', false);
                    $('.pswd-conf').prop('disabled', false);
                    $('.check-pass').val('checked');
                    $('#akses_pswd').show();
                } else {
                    $('#password').prop('disabled', true);
                    $('.pswd-conf').prop('disabled', true);
                    $('.check-pass').val('');
                    $('#akses_pswd').hide();
                }
            });

            $('#akses_form').on('submit', function(event) {
                event.preventDefault();

                var form_data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:"{{ route('guru.siswa.updateAkses') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if (data.error.length > 0) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                            }
                            $('#error_output').html(error_html);
                            $('#password').val('');
                            $('#password-confirm').val('');
                            $('#error_output').show();
                        } else {
                            $('#form_output').html(data.success);
                            $('#akses_form')[0].reset();
                            $('#button_action').val('update');
                            $('#siswa_table').DataTable().ajax.reload();
                            $('#akses_modal').modal('hide');
                            $('#form_output').show();
                            $('#error_output').hide();
                            $('#action').show();
                            $('#password').prop('disabled', true);
                            $('.pswd-conf').prop('disabled', true);
                            $('#akses_pswd').hide();
                        }
                    }
                })
            });

            $('#profil_form').on('submit', function(event) {
                event.preventDefault();

                var form_data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:"{{ route('guru.siswa.updateProfil') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if (data.error.length > 0) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                            }
                            $('#error_profil').html(error_html);
                            $('#error_profil').show();
                        } else {
                            $('#form_output').html(data.success);
                            $('#profil_form')[0].reset();
                            $('#button_act').val('updated');
                            $('#siswa_table').DataTable().ajax.reload();
                            $('#profil_modal').modal('hide');
                            $('#form_output').show();
                            $('#error_profil').hide();
                            $('#act').show();
                        }
                    }
                })
            });

            $(document).on('click', '.detail', function() {
                var id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('guru.siswa.fetchdata') }}",
                    method:"GET",
                    data:{id:id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#name').val(data.name);
                        $('#email').val(data.email);
                        $('#username').val(data.username);
                        $('#gender').val(data.gender);
                        $('#no_induk').val(data.no_induk);
                        $('#class').val(data.class);
                        $('.modal-title').text('Detail Profil Siswa');
                        $('#id').val(id);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.edit-akses', function() {
                var id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('guru.siswa.fetchdataAkses') }}",
                    method:"GET",
                    data:{id:id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#nameAkses').val(data.name);
                        $('#emailAkses').val(data.email);
                        $('#usernameAkses').val(data.username);
                        $('#pswd_hide').val(data.pswd_hide);
                        $('#action').val('Edit');
                        $('.modal-title').text('Edit Akses User');
                        $('#id').val(id);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.edit-profil', function() {
                var user_id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('guru.siswa.fetchdataProfil') }}",
                    method:"GET",
                    data:{id:user_id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#id_prof').val(data.id);
                        $('#genderProf').val(data.gender);
                        $('#genderProf').val(data.gender);
                        $('#no_indukProf').val(data.no_induk);
                        $('#class_id').val(data.class_id);
                        $('#user_id').val(data.user_id);
                        $('#act').val('Edit');
                        $('.modal-title').text('Edit Profil Siswa');
                        $('#id_pfl').val(user_id);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).attr('id');
                if(confirm("Apakah anda yakin akan menghapus Data ini?"))
                {
                    $.ajax({
                        url:"{{ route('guru.siswa.delete') }}",
                        method:"GET",
                        data:{id:id},
                        success:function(data)
                        {
                            alert(data);
                            $('#siswa_table').DataTable().ajax.reload();
                            $('#form_output, #error_output, #error_profil').hide();
                        }
                    }) 
                }
                else {
                    return false;
                }
            });
        });
    </script>
@stop
