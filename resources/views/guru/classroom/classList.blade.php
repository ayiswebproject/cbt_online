@extends('guru.layouts.header-footer')

@section('css-app')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('sidebar-app')
    <li class="nav-item">
      <a class="nav-link" href="{{ route('homeGuru') }}">
        <i class="fas fa-fw fa-home"></i>
        <span>Home</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
      Menu
    </div>

    <li class="nav-item active">
      <a class="nav-link" href="{{ route('guru.kelas') }}">
        <i class="fas fa-fw fa-chalkboard"></i>
        <span>Manajemen Kelas</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru.siswa') }}">
        <i class="fas fa-fw fa-user-tie"></i>
        <span>Manajemen Siswa</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('guru.ujian') }}">
        <i class="fas fa-fw fa-book-reader"></i>
        <span>Manajemen Ujian</span></a>
    </li>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Manajemen Kelas</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Data Kelas</h6>
          <button type="button" id="add" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#class_modal">
            <i class="fas fa-plus"></i>
            Tambah
          </button>
        </div>
        <div class="card-body">
          <span id="form_output"></span>
          <div class="table-responsive">
            <table class="table table-bordered" id="class_table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nama Kelas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    <div id="class_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="add_class_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Data Kelas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nama Kelas<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="class" name="class" class="form-control" autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="insert">
                        <input type="submit" name="submit" id="action" value="Tambah" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {            
            $('#class_table').DataTable({
                "processing": true,
                "serverSide": true,
                'bSort': false,
                "ajax": "{{ route('guru.kelas.getData') }}",
                "columns": [
                    { 
                        "data": "class",
                        "width": "30%"
                    },
                    { 
                        "data": "action",
                        "orderable": "false",
                        "searchable": "false",
                        "width": "30%"
                    }
                ]
            });

            $('.close, .close-btn').click(function() {
                $('#error_output').hide();
            });

            $('#add').click(function() {
                $('#add_class_form')[0].reset();
                $('#form_output').html('');
                $('#button_action').val('insert');
                $('#action').val('Tambah');
                $('.modal-title').html('Tambah Data Kelas');
                $('#class').prop('readonly', false);
                $('#form_output').hide();
                $('#action').show();
            });

            $('#add_class_form').on('submit', function(event) {
                event.preventDefault();

                var form_data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:"{{ route('guru.kelas.store') }}",
                    method:"POST",
                    data:form_data,
                    dataType:"json",
                    success:function(data)
                    {
                        if (data.error.length > 0) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                            }
                            $('#error_output').html(error_html);
                            $('#error_output').show();
                        } else {
                            $('#form_output').html(data.success);
                            $('#add_class_form')[0].reset();
                            $('#button_action').val('insert');
                            $('#class_table').DataTable().ajax.reload();
                            $('#action').show();
                            $('#class').prop('readonly', false);
                            $('#form_output').show();
                            $('#error_output').hide();
                            $('#class_modal').modal('hide');
                        }
                    }
                })
            });

            $(document).on('click', '.detail', function() {
                var id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('guru.kelas.fetchdata') }}",
                    method:"GET",
                    data:{id:id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#class').val(data.class);
                        $('#id').val(id);
                        $('.modal-title').text('Detail Data Kelas');
                        $('#action').hide();
                        $('#class').prop('readonly', true);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).attr('id');
                $.ajax({
                    url:"{{ route('guru.kelas.fetchdata') }}",
                    method:"GET",
                    data:{id:id},
                    dataType:"json",
                    success:function(data)
                    {
                        $('#class').val(data.class);
                        $('#id').val(id);
                        $('#action').val('Edit');
                        $('.modal-title').text('Edit Data Kelas');
                        $('#button_action').val('update');
                        $('#action').show();
                        $('#class').prop('readonly', false);
                        $('#form_output').hide();
                    }
                })
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).attr('id');
                if(confirm("Apakah anda yakin akan menghapus Data ini?"))
                {
                    $.ajax({
                        url:"{{ route('guru.kelas.delete') }}",
                        method:"GET",
                        data:{id:id},
                        success:function(data)
                        {
                            alert(data);
                            $('#class_table').DataTable().ajax.reload();
                            $('#form_output, #error_output').hide();
                        }
                    }) 
                }
                else {
                    return false;
                }
            });
        });
    </script>
@stop
