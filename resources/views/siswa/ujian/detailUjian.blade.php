@extends('siswa.layouts.header-footer')

@section('css-app')
    <style type="text/css">
        #main-content h1 {
            text-align: center;
            font-style: italic;
            font-size: 72pt;
        }
        @media (max-width: 600px) {
            #main-content h1 {
                font-size: 42px;
            }
        }
        #main-content h5 {
            text-align: center;
            font-size: 18pt;
        }
        @media (max-width: 600px) {
            #main-content h5 {
                font-size: 16px;
            }
        }
        #main-content .left {
            float: right;
        }
        .col-sm-6 {
            margin-bottom: 25px;
        }
        .brand-card {
            padding: 35px 35px;
        }
        @media (max-width: 600px) {
            .brand-card {
                padding: 0px 0px;
            }
        }
        .btn-nav {
            padding: 200px 260px;
        }
        .btn-back {
            float: left;
        }
        .btn-next {
            float: right;
        }
        .brand-card {
            padding: 88px 125px;
        }
        #start_exam_card .back {
            float: right;
            display: block;
        }
        #start_exam_card .next {
            float: left;
        }
        .form-group .form-control,
        .form-group label {
            font-size: 12px;
        }
        .form-group .row {
            margin-bottom: 25px;
        }
        .card.bg-red {
            background-color: red;
            padding: 15px;
        }
        .card.bg-red .card-body {
            border: 3px dashed #fff;
            border-radius: 2px;
        }
        .card.bg-red h5 {
            font-weight: bold;
            color: #fff;
        }
        .card.bg-red li {
            color: #fff;
        }
    </style>
@stop

@section('no-back-page')
<script type="text/javascript">
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Detail Ujian</h1>

      <div class="row" id="main-content">
            @foreach($detail as $details)
                <style type="text/css">
                    #warning {
                        display: none;
                    }
                </style>
                <div class="col-sm-6">
                    <div class="card shadow">
                        <div class="card-header">
                            <h6 class="m-0 font-weight-bold text-primary">Data Ujian</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label id="code_test">Kode Ujian</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input for="code_test" type="text" name="code_test" class="form-control" value="{{ $details->code_test }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label id="name_test">Nama Ujian</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input for="name_test" type="text" name="name_test" class="form-control" value="{{ $details->name_test }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label id="duration">Durasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input for="duration" type="text" name="duration" class="form-control" value="{{ $details->duration }} menit" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label id="type_test">Tipe Ujian</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input for="type_test" type="text" name="type_test" class="form-control" value="{{ $details->type_test }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label id="jml_soal">Jumlah Soal</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input for="jml_soal" type="text" name="jml_soal" class="form-control" value="{{ $details->jml_soal }} soal" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="card border-left-primary shadow h-100 py-2" id="start_exam_card">
                    <div class="card-body">
                        <div class="brand-card">
                            <h2 class="text-center">Mulai ujian sekarang?</h2>
                            <br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{ route('detail.destroy', $details->code_test ) }}" class="back btn btn-secondary col-sm-12">
                                        <i class="fa fa-arrow-left"></i>
                                        &nbsp;Kembali
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{ route('startUjian', $details->code_test ) }}" class="start btn btn-primary col-sm-12">
                                        <i class="fa fa-check"></i>
                                        &nbsp;Mulai Ujian
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            @endforeach
            <div class="col-sm-12">
                <div class="card shadow bg-red">
                    <div class="card-body">
                        <h5 style="text-align: left;">Silahkan kerjakan soal yang telah di siapkan. Harap dipatuhi peraturan berikut:</h5>
                        <ul>
                            <li>Jangan mereload/refresh browser (jawaban akan hilang dan ujian mengulang dari awal)</li>
                            <li>Jangan menekan tombol selesai saat mengerjakan soal, kecuali saat anda telah selesai mengerjakan seluruh soal</li>
                            <li>Perhatikan sisa waktu ujian, sistem akan mengumpulkan jawaban saat waktu sudah selesai</li>
                            <li>Waktu ujian akan dimulai saat tombol <b>"Mulai Ujian"</b> di klik</li>
                            <li>Dilarang bekerja sama dengan teman</li>
                        </ul>
                    </div>
                </div>
            </div>

    </div>
    <br>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
