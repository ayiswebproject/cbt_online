@extends('siswa.layouts.header-footer')

@section('css-app')
<style type="text/css">
    #main-content h1 {
        text-align: center;
        font-style: italic;
        font-size: 72pt;
    }
    @media (max-width: 600px) {
        #main-content h1 {
            font-size: 42px;
        }
    }
    #main-content h5 {
        text-align: center;
        font-size: 18pt;
    }
    @media (max-width: 600px) {
        #main-content h5 {
            font-size: 16px;
        }
    }
    #main-content .left {
        float: right;
    }
    .col-sm-6 {
        margin-bottom: 25px;
    }
    .brand-card {
        padding: 35px 35px;
    }
    @media (max-width: 600px) {
        .brand-card {
            padding: 0px 0px;
        }
    }
    .confirm h5 {
        font-size: 20px !important;
    }
    .confirm img {
        width: 20%;
        display: block;
        margin: 15px auto;
    }
    .confirm {
        padding: 60px 0;
    }
</style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Selesai Ujian</h1>
      <br>
      <div class="row justify-content-center" id="main-content">
          <div class="col-sm-6">
              <div class="card">
                  <div class="card-body confirm">
                    <img src="{{ asset('img/checked.png') }}">
                      <h5>Terimakasih, anda telah menyelesaikan ujian</h5>
                      <br>
                      <a href="{{ route('logout') }}" style="text-align: center; display: block;">
                          <button class="btn btn-primary">Logout</button>
                      </a>
                  </div>
              </div>
          </div>
      </div>

    </div>
    <!-- /.container-fluid -->
    <div style="margin-top: 80px"></div>
@stop

@section('js-app')

@stop
