@extends('siswa.layouts.header-footer')

@section('css-app')
<style type="text/css">
    .bg-nav-soal {
        background-color: #f0f0f0;
        border-radius: 50%;
        padding: 10px 8px 10px 8px;
        box-shadow: 1px 1px 5px #b4b4b4;
        display: block;
        margin: 0 auto;
        margin-bottom: 30px;
    }
    .done {
        background-color: #1CC88A !important;
        color: #fff !important;
    }
    .bg-nav-soal span {
        text-align: center;
        display: block;
    }
    .bg-nav-soal:hover {
        background-color: #c4c4c4;
        color: #fff;
    }
    .active .bg-nav-soal {
        background-color: #c4c4c4;
        color: #fff !important; 
    }
    .nav-keterangan {
        margin-top: 20px;
    }
    .nav-keterangan .badge {
        margin-bottom: 7px;
        margin-right: 7px;
    }
    .nav-keterangan .badge-light {
        background-color: #f0f0f0;
    }
    .nav-keterangan .badge-success {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 11px 10px 11px;
        border-radius: 50%;
    }
    .nav-keterangan .badge-light {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 13px 10px 13px;
        border-radius: 50%;
    }
    .card-header a.btn,
    .card-header button {
        float: right;
        margin-top: -20px;
        color: #fff;
    }
    .numbering {
        vertical-align: text-top;
    }
    .questions .form-check {
        padding: 10px 25px;
    }
    .carousel-indicators {
        position: relative;
        margin-right: 7%;
        margin-left: 7%;
    }
    .carousel-indicators span {
        width: 30px;
    }
    form label {
        font-weight: bold;
    }
    ul.sidebar-dark {
        display: none !important;
    }
    .btn-info {
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
        cursor: pointer;
        color: #fff !important;
    }
    .carousel-inner td {
        font-size: 14px;
    }
    .answer label {
        font-weight: bold;
        font-size: 14px;
    }
    .answer span {
        font-weight: 400;
        font-size: 14px;
    }
    span#key {
        text-transform: uppercase;
    }
    .modal-dialog {
        max-width: 1024px !important;
    }
    .table {
        margin-bottom: 0 !important;
    }
    .cek-a.yes,
    .cek-b.yes,
    .cek-c.yes,
    .cek-d.yes,
    .cek-e.yes {
        cursor: pointer;
        color: #5a5c69;
        font-weight: bold;
        border: 1px solid #5a5c69;
        padding: 3px 8px;
        border-radius: 50%;
        background-color: #fff;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript">
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};

    document.onkeydown = function() {    
        switch (event.keyCode) { 
            case 116 : //F5 button
                event.returnValue = false;
                event.keyCode = 0;
                return false; 
            case 82 : //R button
                if (event.ctrlKey) { 
                    event.returnValue = false; 
                    event.keyCode = 0;  
                    return false; 
                } 
        }
    }
</script>
@stop

@section('timer-exam')
    <li class="nav-item">
      <span class="timer">
          <div class="card" style="border-radius: 0;">
              <div class="card-body">
                  Waktu Tersisa :
                  <b style="font-size:1.2em" id="clock">
                      <span class="hours"></span>:<span class="minutes"></span>:<span class="seconds"></span>
                  </b>
              </div>
          </div>
      </span>
    </li>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Soal Ujian</h1>
                <br>

                @if(Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                    </div>
                @endif
                @if(Session::has('alert-info'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                    </div>
                @endif
                @if(Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                    </div>
                @endif

                <!-- DataTales Example -->
                <div class="alert alert-danger" id="alert-end" style="display: none;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Waktu ujian telah habis, ujian telah selesai.</div>
                <div id="soalNavigation" class="carousel slide row" data-ride="carousel" data-interval="false">
                    <div class="col-sm-4">
                        <div class="card shadow mb-4" id="data_soal">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Navigasi Soal</h6>
                            </div>
                            <div class="card-body">
                                <div style="overflow-y: auto; height: 325px;">
                                    <div class="container" style="margin: 10px 0px 10px 0px;">
                                        <div class="row carousel-indicators">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach($soal as $soals)
                                                <div class="col-sm-3 {{ $loop->first ? 'active' : '' }} selector">
                                                    <a data-target="#soalNavigation" data-slide-to="{{ $loop->index }}" class="navi nav{{ $i }}">
                                                        <button class="btn bg-nav-soal">
                                                            <span>{{ $i }}</span>
                                                        </button>
                                                    </a>
                                                </div>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="nav-keterangan">
                                    <div class="ket-done">
                                        <span class="badge badge-success">
                                            <i class="fas fa-check"></i>
                                        </span> Soal Sudah Dijawab
                                    </div>
                                    <div class="ket-false">
                                        <span class="badge badge-light">
                                            <i class="fas fa-times"></i>
                                        </span> Soal Belum Dijawab
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Soal Ujian</h6>
                            </div>
                            <div class="card-body">
                                @foreach($soal as $soals)
                                <form action="@if( $soals->type_test == 'Pilihan Ganda'){{ route('storeJawaban') }}@else{{ route('storeJawabanEssay') }}@endif" method="POST" id="form-ujian">
                                @endforeach
                                    {{ csrf_field() }}
                                    <input type="hidden" name="jml_soal" value="{{ $soals->jml_soal }}">
                                    <div class="container" id="soal" style="margin: 10px 0px 10px 0px;">
                                      <span id="form_output"></span>
                                      <div class="carousel-inner">
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach($soal as $soals)
                                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                                <table width="100%" id="soal-list">
                                                    <tr>
                                                        <td class="numbering" width="7%">
                                                            <h5>{{ $no }}. </h5>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="question[{{ $no }}]" value="{{ $soals->id }}">
                                                            <span id="questions_view">{{ $soals->questions }}</span>
                                                            <br><br>
                                                            @if( $soals->quest_img != null )
                                                                <img src="{{ asset('uploads/soal/'.$soals->quest_img) }}" style="width: 20%"><br><br>
                                                            @else

                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if( $soals->type_test == 'Pilihan Ganda')
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div class="answer">
                                                                    <table class="table table-borderless" width="100%">
                                                                        <tr>
                                                                            <td width="5%">
                                                                                <div class="link-a">
                                                                                    <a class="cek-a yes">A</a>
                                                                                </div>
                                                                                <div class="a-key">
                                                                                    <input type="hidden" name="choice[{{ $no }}]" value="">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <span id="a_view">{{ $soals->a }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div class="answer">
                                                                    <table class="table table-borderless" width="100%">
                                                                        <tr>
                                                                            <td width="5%">
                                                                                <div class="link-b">
                                                                                    <a class="cek-b yes">B</a>
                                                                                </div>
                                                                                <div class="b-key">
                                                                                    <input type="hidden" name="choice[{{ $no }}]" value="">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <span id="b_view">{{ $soals->b }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div class="answer">
                                                                    <table class="table table-borderless" width="100%">
                                                                        <tr>
                                                                            <td width="5%">
                                                                                <div class="link-c">
                                                                                    <a class="cek-c yes">C</a>
                                                                                </div>
                                                                                <div class="c-key">
                                                                                    <input type="hidden" name="choice[{{ $no }}]" value="">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <span id="c_view">{{ $soals->c }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div class="answer">
                                                                    <table class="table table-borderless" width="100%">
                                                                        <tr>
                                                                            <td width="5%">
                                                                                <div class="link-d">
                                                                                    <a class="cek-d yes">D</a>
                                                                                </div>
                                                                                <div class="d-key">
                                                                                    <input type="hidden" name="choice[{{ $no }}]" value="">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <span id="d_view">{{ $soals->d }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <div class="answer">
                                                                    <table class="table table-borderless" width="100%">
                                                                        <tr>
                                                                            <td width="5%">
                                                                                <div class="link-e">
                                                                                    <a class="cek-e yes">E</a>
                                                                                </div>
                                                                                <div class="e-key">
                                                                                    <input type="hidden" name="choice[{{ $no }}]" value="">
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <span id="e_view">{{ $soals->e }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @else
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <textarea class="form-control" id="choice_essay" name="choice_essay[{{ $no }}]" val="" rows="5" placeholder="Jawaban"></textarea>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </table>
                                                <br>
                                                <br>
                                                <div class="dropdown-divider"></div>
                                            </div>
                                            @php
                                                $no++
                                            @endphp
                                        @endforeach
                                      </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6" style="margin-bottom: 0">
                                                <a href="#soalNavigation" id="prev" data-slide="prev" class="btn btn-secondary" style="float: left;">
                                                    <i class="fas fa-chevron-left"></i>
                                                    Sebelumnya
                                                </a>
                                                <a class="btn btn-warning" id="warning" style="float: right; cursor: pointer; color: #fff;">
                                                    <i class="fas fa-exclamation-circle"></i>
                                                    Ragu-Ragu
                                                </a>
                                            </div>
                                            <div class="col-sm-6" style="margin-bottom: 0">
                                                <button class="btn btn-success" style="float: left;">
                                                    <i class="fas fa-check"></i>
                                                    Selesai
                                                </button>
                                                <a href="#soalNavigation" id="next" data-slide="next" class="btn btn-primary" style="float: right;">
                                                    Selanjutnya
                                                    <i class="fas fa-chevron-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
    @foreach($timer as $timers)
        <script type="text/javascript">
            $(document).ready(function () {
                // $(window).bind("beforeunload",function(event) {
                //     return "You have some unsaved changes";
                // });             

                // Option A
                $('.cek-a').on('click', function() {
                    $(this).parent().next('.a-key').find('input').prop('disabled', false);
                    $(this).parent().next('.a-key').find('input').val('a');
                    $(this).css('background-color', '#1CC88A');
                    $(this).css('color', '#FFF');
                    $(this).css('border-color', '#1CC88A');

                    // disabled Option B
                    $(this).parents('table').find('.cek-b').parent().next('.b-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-b').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-b').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-b').css('border-color', '#5a5c69');

                    // disabled Option C
                    $(this).parents('table').find('.cek-c').parent().next('.c-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-c').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-c').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-c').css('border-color', '#5a5c69');

                    // disabled Option D
                    $(this).parents('table').find('.cek-d').parent().next('.d-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-d').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-d').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-d').css('border-color', '#5a5c69');

                    // disabled Option E
                    $(this).parents('table').find('.cek-e').parent().next('.e-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-e').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-e').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-e').css('border-color', '#5a5c69');
                });

                // Option B
                $('.cek-b').on('click', function() {
                    $(this).parent().next('.b-key').find('input').prop('disabled', false);
                    $(this).parent().next('.b-key').find('input').val('b');
                    $(this).css('background-color', '#1CC88A');
                    $(this).css('color', '#FFF');
                    $(this).css('border-color', '#1CC88A');

                    // disabled Option A
                    $(this).parents('table').find('.cek-a').parent().next('.a-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-a').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-a').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-a').css('border-color', '#5a5c69');

                    // disabled Option C
                    $(this).parents('table').find('.cek-c').parent().next('.c-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-c').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-c').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-c').css('border-color', '#5a5c69');

                    // disabled Option D
                    $(this).parents('table').find('.cek-d').parent().next('.d-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-d').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-d').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-d').css('border-color', '#5a5c69');

                    // disabled Option E
                    $(this).parents('table').find('.cek-e').parent().next('.e-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-e').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-e').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-e').css('border-color', '#5a5c69');
                });

                // Option C
                $('.cek-c').on('click', function() {
                    $(this).parent().next('.c-key').find('input').prop('disabled', false);
                    $(this).parent().next('.c-key').find('input').val('c');
                    $(this).css('background-color', '#1CC88A');
                    $(this).css('color', '#FFF');
                    $(this).css('border-color', '#1CC88A');

                    // disabled Option B
                    $(this).parents('table').find('.cek-b').parent().next('.b-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-b').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-b').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-b').css('border-color', '#5a5c69');

                    // disabled Option A
                    $(this).parents('table').find('.cek-a').parent().next('.a-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-a').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-a').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-a').css('border-color', '#5a5c69');

                    // disabled Option D
                    $(this).parents('table').find('.cek-d').parent().next('.d-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-d').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-d').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-d').css('border-color', '#5a5c69');

                    // disabled Option E
                    $(this).parents('table').find('.cek-e').parent().next('.e-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-e').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-e').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-e').css('border-color', '#5a5c69');
                });

                // Option D
                $('.cek-d').on('click', function() {
                    $(this).parent().next('.d-key').find('input').prop('disabled', false);
                    $(this).parent().next('.d-key').find('input').val('d');
                    $(this).css('background-color', '#1CC88A');
                    $(this).css('color', '#FFF');
                    $(this).css('border-color', '#1CC88A');

                    // disabled Option B
                    $(this).parents('table').find('.cek-b').parent().next('.b-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-b').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-b').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-b').css('border-color', '#5a5c69');

                    // disabled Option C
                    $(this).parents('table').find('.cek-c').parent().next('.c-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-c').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-c').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-c').css('border-color', '#5a5c69');

                    // disabled Option A
                    $(this).parents('table').find('.cek-a').parent().next('.a-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-a').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-a').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-a').css('border-color', '#5a5c69');

                    // disabled Option E
                    $(this).parents('table').find('.cek-e').parent().next('.e-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-e').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-e').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-e').css('border-color', '#5a5c69');
                });

                // Option E
                $('.cek-e').on('click', function() {
                    $(this).parent().next('.e-key').find('input').prop('disabled', false);
                    $(this).parent().next('.e-key').find('input').val('e');
                    $(this).css('background-color', '#1CC88A');
                    $(this).css('color', '#FFF');
                    $(this).css('border-color', '#1CC88A');

                    // disabled Option B
                    $(this).parents('table').find('.cek-b').parent().next('.b-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-b').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-b').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-b').css('border-color', '#5a5c69');

                    // disabled Option C
                    $(this).parents('table').find('.cek-c').parent().next('.c-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-c').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-c').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-c').css('border-color', '#5a5c69');

                    // disabled Option D
                    $(this).parents('table').find('.cek-d').parent().next('.d-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-d').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-d').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-d').css('border-color', '#5a5c69');

                    // disabled Option A
                    $(this).parents('table').find('.cek-a').parent().next('.a-key').find('input').prop('disabled', true);
                    $(this).parents('table').find('.cek-a').css('background-color', '#FFF');
                    $(this).parents('table').find('.cek-a').css('color', '#5a5c69');
                    $(this).parents('table').find('.cek-a').css('border-color', '#5a5c69');
                });

                $('#selesai').click(function() {
                    var count = 0;
                    $('textarea').each(function(){
                        if ($(this).val() == "") {
                            alert('gagal');
                            return false;
                        }
                    });
                });

                $(document).on('click', 'a.yes', function() {
                    $('.active .bg-nav-soal').css('background-color', '#1CC88A');
                    $('.active .bg-nav-soal').css('color', '#fff');
                });

                $(document).on('change', 'textarea', function() {
                    $('.active .bg-nav-soal').css('background-color', '#1CC88A');
                    $('.active .bg-nav-soal').css('color', '#fff');
                });

                $(document).on('click', '#warning', function() {
                    $('.active .bg-nav-soal').css('background-color', '#F6C23E');
                    $('.active .bg-nav-soal').css('color', '#fff');
                });

                var soalnya = $('#soal');

                var waktuHabis = function () {  

                    $('#soalNavigation').hide();
                    $('#alert-end').show();

                    setTimeout(function () {

                        $('#form-ujian').submit();
                        
                    }, 5000);
                }

                /**
                * COUNTDOWN TIMER
                *
                */
                function getTimeRemaining(endtime) {
                  var t = Date.parse(endtime) - Date.parse(new Date());
                  var seconds = Math.floor((t / 1000) % 60);
                  var minutes = Math.floor((t / 1000 / 60) % 60);
                  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
                  var days = Math.floor(t / (1000 * 60 * 60 * 24));
                  return {
                    'total': t,
                    'days': days,
                    'hours': hours,
                    'minutes': minutes,
                    'seconds': seconds
                  };
                }

                function initializeClock(id, endtime) {
                  var clock = document.getElementById(id);
                  var hoursSpan = clock.querySelector('.hours');
                  var minutesSpan = clock.querySelector('.minutes');
                  var secondsSpan = clock.querySelector('.seconds');

                  function updateClock() {
                    var t = getTimeRemaining(endtime);

                    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                    if (t.total <= 0) {
                        clearInterval(timeinterval);
                        waktuHabis();
                    } else if(t.total == 5 * 60 *  1000) {

                        soalnya.prepend('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Waktu tinggal 5 menit lagi. Segera periksa pekerjaan kamu. Sistem akan secara otomatis mengumpulkan pekerjaanmu setelah waktu habis.</div>');
                    }
                  }

                  updateClock();
                  var timeinterval = setInterval(updateClock, 1000);
                }

                var deadline = new Date(Date.parse(new Date()) + {{ $timers->duration }} * 60 * 1000);
                initializeClock('clock', deadline);
            });
        </script>
    @endforeach
@stop
