@extends('layouts.auth-header')

@section('css-app')
<style type="text/css">
  .login-size {
    padding: 100px 50px;
  }
  .bg-login {
    background-image: url("{{ asset('img/bg_login.jpg') }}");
    background-size: cover;
    background-repeat: no-repeat;
  }
  .logo-login {
    width: 45%;
    margin: 25px auto 0 auto;
    display: block;

  }
</style>
@stop

@section('main-app')
  <div class="container">

      <!-- Outer Row -->
      <div class="row justify-content-center">

        <div class="col-sm-6">

        <img src="{{ asset('img/ayocbt_dark.png') }}" class="logo-login">

          <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
              <!-- Nested Row within Card Body -->
              <div class="row">
                <div class="col-sm-12">
                  <div class="login-size">
                    <div class="text-center">
                      <h1 class="h4 text-gray-900 mb-4" style="text-transform: uppercase;"><b>Login</b></h1>
                    </div>
                    <form class="user" method="POST" action="{{ route('login') }}">
                      @csrf
                      <div class="form-group">
                        <input type="text" class="form-control form-control-user{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}" id="exampleInputEmail" aria-describedby="emailHelp" name="login" value="{{ old('username') ?: old('email') }}" placeholder="Username / Email" required>
                        @if ($errors->has('username') || $errors->has('email'))
  	                        <span class="invalid-feedback" role="alert">
  	                            <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
  	                        </span>
  	                    @endif
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" id="exampleInputPassword" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>
                      <button class="btn btn-primary btn-user btn-block" style="text-transform: uppercase;">
                        Login
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>

    </div>
@stop

@section('js-app')

@stop