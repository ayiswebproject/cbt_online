// Call the dataTables jQuery plugin


$(document).ready(function() {
	$('#dataTable2').dataTable({
	  bAutoWidth: false, 
	  aoColumns : [
	    { sWidth: '1%' },
	    { sWidth: '15%' },
	    { sWidth: '7%' },
	    { sWidth: '12%' },
	    { sWidth: '10%' },
	    { sWidth: '10%' },
	    { sWidth: '12%' },
	    { sWidth: '15%' }
	  ]
	});
});

$(document).ready(function() {
	$('#dataTable3').dataTable({
	  bAutoWidth: false, 
	  aoColumns : [
	    { sWidth: '1%' },
	    { sWidth: '15%' },
	    { sWidth: '15%' },
	    { sWidth: '7%' },
	    { sWidth: '12%' },
	    { sWidth: '15%' },
	    { sWidth: '15%' },
	    { sWidth: '35%' }
	  ]
	});
});
