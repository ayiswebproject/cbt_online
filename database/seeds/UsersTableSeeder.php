<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin CBT';
        $user->username = 'admin_cbt';
        $user->email = 'admin_cbt@gmail.com';
        $user->password = bcrypt('admin123');
        $user->role = 'admin';
        $user->save();
    }
}
